<#
.SYNOPSIS
    Update the PowerShell modules and scripts by pulling latest from git remote
#>
param([switch]$BypassGit)

if (!$BypassGit)
{
    $scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition
    Push-Location $scriptPath
    try 
    {
        Write-Host 'Updating the PowerShell modules and scripts by pulling latest from git remote'

        git fetch

        $log = git log ..origin/master --oneline

        if ($log -ne $null -and $log.Length -gt 0)
        {
            git rebase
        }
    }
    finally
    {
        Pop-Location
    }
}