$scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition
$pathEnvironmentAssignment = '$env:Path = $env:Path' + ' + ";' + $scriptPath + '"'
$modulePathEnvironmentAssignment = '$env:PSModulePath = $env:PSModulePath' + ' + ";' + $scriptPath + '"'
$autoUpdateExpression = 'Update-MyModule'
$devModule = 'Import-Module dev'

if (!(Test-Path $PROFILE))
{
    Write-Verbose "Creating PowerShell profile..."
    New-Item $PROFILE -Type File -Force -ErrorAction Stop | Out-Null
}

if (!(Select-String $PROFILE -Pattern $pathEnvironmentAssignment -SimpleMatch))
{
    "`n$pathEnvironmentAssignment" | Add-Content $PROFILE
    Invoke-Expression $pathEnvironmentAssignment
    Write-Host "Your path has been updated to include $scriptPath"
}

if (!(Select-String $PROFILE -Pattern $modulePathEnvironmentAssignment -SimpleMatch))
{
    "`n$modulePathEnvironmentAssignment" | Add-Content $PROFILE
    Invoke-Expression $modulePathEnvironmentAssignment
    Write-Host "Your PowerShell module path has been updated to include $scriptPath"
}

if (!(Select-String $PROFILE -Pattern $autoUpdateExpression -SimpleMatch))
{
    "`n$autoUpdateExpression" | Add-Content $PROFILE
    Write-Host "Your PowerShell profile has been updated to automatically update the PowerShell modules"
}

if (!(Select-String $PROFILE -Pattern $devModule -SimpleMatch))
{
    "`n$devModule" | Add-Content $PROFILE
    Write-Host "Dev module auto load added to your profile"
}