function SayHello
{
    Write-Host "Hello World"
}

function New-Migration {
    param (
        [string] $Name,
        [switch] $JournalOnly,
        [switch] $WithReload
    )

   $projectDirectory = $PWD.Path
   $scriptDirectory = $projectDirectory + "\Migrations"
   $fileNameBase = (Get-Date -UFormat "%y%m%d%H%M%S")
 
   #If Git branch name is available, change target project item to Migrations\[GitBranch] (nested folder)
   $gitBranch = Get-Branch
   if ($gitBranch -ne $null){
       $scriptDirectory = $scriptDirectory + "\" + $gitBranch 
   }

   If ($name -ne ""){
    $fileNameBase = $fileNameBase + "_" + $Name
   }

   $fileNameBase = $fileNameBase.Replace(" ","")
   $fileName = $fileNameBase + ".sql"
   $filePath = $scriptDirectory + "\" + $fileName

   New-Item -path $scriptDirectory -name $fileName -type "file" -value "/* Migration Script */" -Force | Out-Null

   Write-Host "Created new migration: ${fileName}"

   if ($JournalOnly.IsPresent){
        Start-Migrations -JournalOnly -ScriptFile $fileName
   }
}

function Start-Migrations {
    param (
     [switch] $WhatIf,
     [switch] $JournalOnly,
     [string] $ScriptFile,
     [switch] $noTransaction,
     [switch] $Force,
     [switch] $CurrentBranchOnly,
     [string] $Branch,
     [switch] $All
    )

    $projectName = Split-Path $PWD.Path -Leaf
    $csproj = ".\" + $projectName + ".csproj"

    Write-Host "Building..."
    $msbuild = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
    if (-Not (Test-Path $msbuild)){
      Write-Host "MsBuild not found. Make sure you have MsBuild installed at this location: " $msbuild  
      return
    }

    iex $msbuild $csproj /p:Configuration=Release /verbosity:minimal /tv:4.0

    $projectDirectory = $PWD.Path
    
    $args = " --scriptDefinitions"

    if ($Whatif.IsPresent){
        $args = $args + " --whatif"
    }

    if ($JournalOnly.IsPresent){
        $args = $args + " --journalOnly"
    }

    if ($ScriptFile -ne $null){
        $args = $args + " --file=" + $ScriptFile
    }

    if($noTransaction.IsPresent) {
        $args = $args + " --noTransaction"
    }

    if ($Force.IsPresent) {
        $args = $args + " --force"
    }

    if ($All.IsPresent) {
        $args = $args + " --all"
    }
    
    if ($CurrentBranchOnly.IsPresent -or $Force.IsPresent -or $Branch) {
        if ($Branch) {
            $args = $args + " --branch=" + $Branch
        } else {
            $gitBranch = Get-Branch
            $args = $args + " --branch=" + $gitBranch
        }
    }
    $projectExe = $projectDirectory + "\bin\Release\" + $projectName + ".exe" + $args
    iex $projectExe
 }

 function Start-DatabaseScript {
    $projectName = Split-Path $PWD.Path -Leaf
    $projectDirectory = pwd
    $projectExe = $projectDirectory + "\bin\Release\" + $projectName + ".exe" + " --scriptAllDefinitions"
    Write-Host "Scripting..."
    iex $projectExe
 }

function Get-Branch {
    $branch = $null
    try{
        git branch | foreach {
            if ($_ -match "^\* (.*)"){
                $branch += $matches[1]
            }
        }
    }
    catch {
        $branch = $null
    }
     return $branch
}